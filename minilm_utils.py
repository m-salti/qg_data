import pandas as pd
import json


def convert_to_qg(fn, fn_out=None):
    with open(fn, "r") as f:
        data = json.load(f)["data"]

    qg_data = [
        {
            "src": paragraph["context"].strip(),
            "tgt": paragraph["qas"][0]["question"].strip(),
        }
        for entry in data
        for paragraph in entry["paragraphs"]
    ]

    df = pd.DataFrame(qg_data)
    if fn_out:
        df.to_json(fn_out, orient="records", lines=True)
    return df


def convert_to_qg_multi(fn, fn_out=None):
    with open(fn, "r") as f:
        data = json.load(f)["data"]

    qg_data = [
        {
            "src": paragraph["context"].strip(),
            "tgt": qa["question"].strip(),
        }
        for entry in data
        for paragraph in entry["paragraphs"]
        for qa in paragraph["qas"]
    ]

    df = pd.DataFrame(qg_data)
    if fn_out:
        df.to_json(fn_out, orient="records", lines=True)
    return df
